package org.mercury_im.messenger.core.di;

import org.mercury_im.messenger.core.NotificationManager;
import org.mercury_im.messenger.core.centers.ConnectionCenter;
import org.mercury_im.messenger.persistence.repository.AccountRepository;
import org.mercury_im.messenger.persistence.repository.ChatRepository;
import org.mercury_im.messenger.persistence.repository.EntityCapsRepository;
import org.mercury_im.messenger.persistence.repository.MessageRepository;
import org.mercury_im.messenger.persistence.repository.RosterRepository;
import org.mercury_im.messenger.core.stores.EntityCapsStore;
import org.mercury_im.messenger.core.stores.PlainMessageStore;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class CenterModule {

    @Singleton
    @Provides
    static ConnectionCenter provideConnectionCenter(EntityCapsStore capsStore,
                                                    PlainMessageStore messageStore,
                                                    AccountRepository accountRepository,
                                                    RosterRepository rosterRepository) {
        return new ConnectionCenter(capsStore, messageStore, accountRepository, rosterRepository);
    }

    @Singleton
    @Provides
    static EntityCapsStore providerEntityCapsStore(EntityCapsRepository entityCapsRepository) {
        return new EntityCapsStore(entityCapsRepository);
    }

    @Singleton
    @Provides
    static PlainMessageStore provideMessageStore(RosterRepository rosterRepository,
                                                 ChatRepository chatRepository,
                                                 MessageRepository messageRepository,
                                                 NotificationManager notificationManager) {
        return new PlainMessageStore(rosterRepository, chatRepository, messageRepository, notificationManager);
    }

}
