# Messenger

## Used Design Methods:

* The app is developed using the MVVM (Model View Viewmodel) pattern using LifeCycle aware ViewModels
* Components are wired together using Dependency Injection (DI) with Dagger 2
* Data is persisted using Googles Room database library
* UI is notified by updates to the data through the use of LiveData

## Building

```
git clone <project-url>
cd <project-directory>
git submodule init && git submodule update
gradle assembleDebug
```

## FAQ

* I want to develop, but lots of `org.jivesoftware.smackx.*` classes cannot be found!
  * You forgot to type `git submodule init && git submodule update` as mentioned above
* I'm missing `org.mercury_im.messenger.persistence.requery.*` classes???
  * In Android Studio select the `persistence-requery` module and then click "Build -> Make Module 'persistence-requery'".