package org.mercury_im.messenger.persistence.repository;

import org.mercury_im.messenger.thread_utils.ThreadUtils;

import javax.inject.Named;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.Single;
import io.requery.Persistable;
import io.requery.reactivex.ReactiveEntityStore;
import io.requery.reactivex.ReactiveResult;

public abstract class AbstractRepository<E extends Persistable> extends RequeryRepository {


    private final Class<E> modelType;

    protected AbstractRepository(Class<E> modelType,
                                 ReactiveEntityStore<Persistable> data,
                                 @Named(value = ThreadUtils.SCHEDULER_IO) Scheduler subscriberScheduler,
                                 @Named(value = ThreadUtils.SCHEDULER_UI) Scheduler observerScheduler) {
        super(data, subscriberScheduler, observerScheduler);
        this.modelType = modelType;
    }

    // CRUD

    public Single<E> insert(E model) {
        return data().insert(model)
                .subscribeOn(subscriberScheduler())
                .observeOn(observerScheduler());
    }

    public Single<Iterable<E>> insert(Iterable<E> models) {
        return data().insert(models)
                .subscribeOn(subscriberScheduler())
                .observeOn(observerScheduler());
    }

    public Single<E> upsert(E model) {
        return data().upsert(model)
                .subscribeOn(subscriberScheduler())
                .observeOn(observerScheduler());
    }

    public Single<Iterable<E>> upsert(Iterable<E> models) {
        return data().upsert(models)
                .subscribeOn(subscriberScheduler())
                .observeOn(observerScheduler());
    }

    public Single<E> update(E model) {
        return data().update(model)
                .subscribeOn(subscriberScheduler())
                .observeOn(observerScheduler());
    }

    public Single<Iterable<E>> update(Iterable<E> models) {
        return data().update(models)
                .subscribeOn(subscriberScheduler())
                .observeOn(observerScheduler());
    }

    public Observable<ReactiveResult<E>> getAll() {
        return data().select(modelType)
                .get().observableResult()
                .subscribeOn(subscriberScheduler())
                .observeOn(observerScheduler());
    }

    public Completable delete(E model) {
        return data().delete(model)
                .subscribeOn(subscriberScheduler())
                .observeOn(observerScheduler());
    }

    public Completable delete(Iterable<E> models) {
        return data().delete(models)
                .subscribeOn(subscriberScheduler())
                .observeOn(observerScheduler());
    }

    public Single<Integer> deleteAll() {
        return data().delete(modelType)
                .get().single();
    }
}
