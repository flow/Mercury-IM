package org.mercury_im.messenger.persistence.converter;

import org.jxmpp.jid.EntityBareJid;
import org.jxmpp.jid.impl.JidCreate;

import io.requery.Converter;

public class EntityBareJidConverter implements Converter<EntityBareJid, String> {
    @Override
    public Class<EntityBareJid> getMappedType() {
        return EntityBareJid.class;
    }

    @Override
    public Class<String> getPersistedType() {
        return String.class;
    }

    @Override
    public Integer getPersistedSize() {
        return null;
    }

    @Override
    public String convertToPersisted(EntityBareJid jid) {
        return jid == null ? null : jid.asUnescapedString();
    }

    @Override
    public EntityBareJid convertToMapped(Class<? extends EntityBareJid> aClass, String string) {
        return string == null ? null : JidCreate.entityBareFromOrThrowUnchecked(string);
    }
}
