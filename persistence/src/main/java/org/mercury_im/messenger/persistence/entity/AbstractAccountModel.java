package org.mercury_im.messenger.persistence.entity;

import org.jxmpp.jid.EntityBareJid;
import org.mercury_im.messenger.persistence.converter.EntityBareJidConverter;

import io.requery.Column;
import io.requery.Convert;
import io.requery.Entity;
import io.requery.Generated;
import io.requery.Key;
import io.requery.Persistable;
import io.requery.Table;

@Table(name = "accounts")
@Entity
public abstract class AbstractAccountModel implements Persistable {

    @Key @Generated
    long id;

    @Column(nullable = false)
    @Convert(EntityBareJidConverter.class)
    EntityBareJid jid;

    @Column(nullable = false)
    String password;

    boolean enabled;
    
    String rosterVersion;

    @Override
    public String toString() {
        return "Account[" + id + ", " +
                jid + ", " +
                (enabled ? "enabled" : "disabled") + "]";
    }
}
