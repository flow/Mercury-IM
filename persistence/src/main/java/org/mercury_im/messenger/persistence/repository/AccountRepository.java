package org.mercury_im.messenger.persistence.repository;

import org.jxmpp.jid.EntityBareJid;
import org.mercury_im.messenger.persistence.entity.AccountModel;
import org.mercury_im.messenger.thread_utils.ThreadUtils;

import javax.inject.Inject;
import javax.inject.Named;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.requery.Persistable;
import io.requery.reactivex.ReactiveEntityStore;
import io.requery.reactivex.ReactiveResult;

public class AccountRepository extends AbstractRepository<AccountModel> {

    @Inject
    public AccountRepository(ReactiveEntityStore<Persistable> data,
                             @Named(value = ThreadUtils.SCHEDULER_IO) Scheduler subscriberScheduler,
                             @Named(value = ThreadUtils.SCHEDULER_UI) Scheduler observerScheduler) {
        super(AccountModel.class, data, subscriberScheduler, observerScheduler);
    }

    public Observable<ReactiveResult<AccountModel>> getAccountByJid(EntityBareJid jid) {
        return data().select(AccountModel.class)
                .where(AccountModel.JID.eq(jid))
                .get()
                .observableResult()
                .subscribeOn(subscriberScheduler())
                .observeOn(observerScheduler());
    }

    public Observable<ReactiveResult<AccountModel>> getAccount(long accountId) {
        return data().select(AccountModel.class).where(AccountModel.ID.eq(accountId))
                .get().observableResult()
                .subscribeOn(subscriberScheduler()).observeOn(observerScheduler());
    }

    public Completable delete(long accountId) {
        return data().delete(AccountModel.class)
                .where(AccountModel.ID.eq(accountId))
                .get()
                .single()
                .ignoreElement()
                .subscribeOn(subscriberScheduler())
                .observeOn(observerScheduler());
    }
}
