package org.mercury_im.messenger.ui.login;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import org.mercury_im.messenger.MercuryImApplication;
import org.mercury_im.messenger.R;
import org.mercury_im.messenger.persistence.entity.AccountModel;
import org.mercury_im.messenger.ui.login.AccountsFragment.OnAccountListItemClickListener;
import org.mercury_im.messenger.util.AbstractDiffCallback;
import org.mercury_im.messenger.util.ColorUtil;

import java.util.ArrayList;
import java.util.List;

public class AccountsRecyclerViewAdapter extends RecyclerView.Adapter<AccountsRecyclerViewAdapter.ViewHolder> {

    private final List<AccountModel> mValues;
    private final OnAccountListItemClickListener mListener;
    private final AccountsViewModel viewModel;

    public AccountsRecyclerViewAdapter(AccountsViewModel viewModel, OnAccountListItemClickListener listener) {
        mValues = new ArrayList<>();
        mListener = listener;
        this.viewModel = viewModel;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_account, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        AccountModel account = mValues.get(position);
        holder.jid.setText(account.getJid());
        holder.avatar.setColorFilter(ColorUtil.consistentColor(account.getJid().toString()));
        holder.enabled.setChecked(account.isEnabled());
        holder.accountModel = account;
        holder.enabled.setOnCheckedChangeListener((compoundButton, b) -> {
            viewModel.toggleAccountEnabled(account);
        });
        holder.mView.setOnClickListener(v -> {
            if (null != mListener) {
                // Notify the active callbacks interface (the activity, if the
                // fragment is attached to one) that an item has been selected.
                mListener.onAccountListItemClick(holder.accountModel);
            }
        });
        holder.mView.setOnLongClickListener(v -> {
            if (null != mListener) {
                mListener.onAccountListItemLongClick(holder.accountModel);
            }
            return true;
        });
    }

    public void setValues(List<AccountModel> values) {
        DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(new AccountsDiffCallback(values, mValues), true);
        mValues.clear();
        mValues.addAll(values);
        diffResult.dispatchUpdatesTo(this);
    }

    @Override
    public int getItemCount() {
        Log.d(MercuryImApplication.TAG, "Accounts Item Count: " + mValues.size());
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public AccountModel accountModel;
        public final View mView;
        public final ImageView avatar;
        public final TextView jid;
        public final Switch enabled;
        public final TextView status;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            avatar = view.findViewById(R.id.avatar_account);
            jid = view.findViewById(R.id.text_account_jid);
            enabled = view.findViewById(R.id.switch_account_enabled);
            status = view.findViewById(R.id.text_account_status);
        }
    }

    public class AccountsDiffCallback extends AbstractDiffCallback<AccountModel> {

        public AccountsDiffCallback(List<AccountModel> newItems, List<AccountModel> oldItems) {
            super(newItems, oldItems);
        }

        @Override
        public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
            return oldItems.get(oldItemPosition).getId() == newItems.get(newItemPosition).getId();
        }

        @Override
        public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
            AccountModel oldM = oldItems.get(oldItemPosition);
            AccountModel newM = newItems.get(newItemPosition);
            return oldM.equals(newM);
        }
    }
}
