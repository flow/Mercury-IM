package org.mercury_im.messenger.ui.login;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton;

import org.mercury_im.messenger.MercuryImApplication;
import org.mercury_im.messenger.R;
import org.mercury_im.messenger.persistence.entity.AccountModel;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnAccountListItemClickListener}
 * interface.
 */
public class AccountsFragment extends Fragment {

    private OnAccountListItemClickListener accountClickListener;

    AccountsViewModel viewModel;

    private AccountsRecyclerViewAdapter adapter;

    @BindView(R.id.list)
    RecyclerView recyclerView;

    @BindView(R.id.fab)
    ExtendedFloatingActionButton fab;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public AccountsFragment() {
    }

    // TODO: Customize parameter initialization
    public static AccountsFragment newInstance() {
        AccountsFragment fragment = new AccountsFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d(MercuryImApplication.TAG, "AccountsFragment.onCreateView");
        View view = inflater.inflate(R.layout.fragment_account_list, container, false);
        ButterKnife.bind(this, view);
        viewModel = new ViewModelProvider(this).get(AccountsViewModel.class);

        // Set the adapter
        Context context = view.getContext();

        fab.setOnClickListener(v -> startActivity(new Intent(context, LoginActivity.class)));
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        this.adapter = new AccountsRecyclerViewAdapter(viewModel, accountClickListener);
        viewModel.getAccounts().observe(this, roomAccountModels -> adapter.setValues(roomAccountModels));
        recyclerView.setAdapter(adapter);

        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnAccountListItemClickListener) {
            accountClickListener = (OnAccountListItemClickListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnAccountListItemClickListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        accountClickListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnAccountListItemClickListener {
        void onAccountListItemClick(AccountModel item);

        void onAccountListItemLongClick(AccountModel item);
    }
}
