package org.mercury_im.messenger.ui.roster;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;

import org.mercury_im.messenger.R;
import org.mercury_im.messenger.ui.roster.bookmarks.BookmarkListFragment;
import org.mercury_im.messenger.ui.roster.contacts.ContactListFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RosterFragment extends Fragment {

    @BindView(R.id.tab_layout)
    TabLayout tabLayout;

    @BindView(R.id.viewpager)
    ViewPager viewPager;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_roster_tabswitcher, container, false);
        ButterKnife.bind(this, view);

        viewPager.setAdapter(new RosterFragmentPagerAdapter(
                getChildFragmentManager(),
                FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT));
        tabLayout.setupWithViewPager(viewPager);

        return view;
    }

    private class RosterFragmentPagerAdapter extends FragmentPagerAdapter {

        final int PAGE_COUNT = 2;
        final String[] PAGE_TITLES = new String[] {
                getString(R.string.tab_contacts),
                getString(R.string.tab_bookmarks)
        };
        final Fragment[] PAGES = new Fragment[] {
                new ContactListFragment(),
                new BookmarkListFragment()
        };

        public RosterFragmentPagerAdapter(@NonNull FragmentManager fm, int behavior) {
            super(fm, behavior);
        }

        @NonNull
        @Override
        public Fragment getItem(int position) {
            return PAGES[position];
        }

        @Override
        public int getCount() {
            return PAGE_COUNT;
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            return PAGE_TITLES[position];
        }
    };
}
