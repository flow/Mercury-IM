package org.mercury_im.messenger.thread_utils;

/**
 * Name constants used by dagger in combination with the @Named annotation.
 */
public class ThreadUtils {
    /**
     * Name for the database / io thread.
     */
    public static final String SCHEDULER_IO = "DatabaseThread";

    /**
     * Name for the UI / main thread.
     */
    public static final String SCHEDULER_UI = "UIThread";
}
